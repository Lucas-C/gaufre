# Gaufre

This is a simplistic Gopher Server using pure Python3 released under AGPLv3+.

Now less than 250 lines of code, and some docs, but nothing else.


## Why this name ?

In french, "gaufre" is the word for gopher but for a waffle too, I think it was funny.


## Installation

Basic server, basic install :

    cp sample_config.py config.py
    vi|nano|emacs config.py

You can then launch it via supervisor, systemd, whatever...


## Plugins, really ?

Yup, I think that this server just has to serve datas. Your plugins can do much more and
without fattening up the server.


### How ?

To add a plugin, just add a folder with the empty `__init__.py` file in it and a main.py
file too. The main file must have two functions : has_route and request.

Each function will receive one argument : context, which represents the context of the
actual request :

* document_root
* hostname
* port
* route
* argument

The first function *has_route* tells the server if the plugin is able to use this
particular route or not. With this, you can use a plugin only if route starts with a
specific string (like "/forum" maybe ?).

The second function *request* parse the route and answer to it (or not). Thus, the
returned values are a boolean and an array of bytes. The boolean indicates if there is
an answer (even empty). If not, other plugins will be tried and finally the server will
answer. If boolean is True, answer is returned to the client.

An optional function *init* is called in order your plugin can do some initialisation.

And finally, an attribute __priority__ can be used to sort plugins actions in ascending
order (default priority is 99).


### Examples

There are two simple examples of plugin with this code:

The first one is the "Hello world" plugin. Activate it and you can see a link to the
welcoming message to the world.

The second one is more useful, it permits you to use ".gophermap" files inside your
folders. If no __.gophermap__ file is found, then other plugins will be tried or server
default answer.
