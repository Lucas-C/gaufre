# encoding: utf-8

hostname = "localhost"
host = "localhost"
port = 7000
document_root = "/var/gopher/"
plugins = "plugins"
sort = True
strict = False
logging = "gopher.log"
format = "%(asctime)s pid:%(process)s %(filename)s:%(lineno)d [%(levelname)s] %(message)s"

