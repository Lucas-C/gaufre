# encoding: utf-8

from pathlib import Path


def has_route(context):
    return True


def request(context):
    # Windows does not support slash at beginning of path
    if context.route.startswith("/"):
        document_path = context.document_root / context.route[1:]
    else:
        document_path = context.document_root / context.route
    if not Path(document_path).resolve().is_relative_to(context.document_root):
        return False, "", "-"
    if not document_path.exists():
        return False, "", "-"
    elif document_path.is_dir():
        if (document_path / ".gophermap").exists():
            with (document_path / ".gophermap").open("r") as file_handler:
                text = file_handler.read()
                text += ".\r\n"
            return True, text.encode("latin-1"), "i"
    return False, "", "-"
