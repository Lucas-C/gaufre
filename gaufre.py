# encoding: utf-8
"""
Gaufre : Simplistic Gopher Server
Based on RFC 1436 and Python3 only
Author: Mindiell
Licence: AGPLv3+
"""

from datetime import datetime
from importlib import import_module
import logging
from pathlib import Path
from selectors import select
import socket
import sys
import config


VERSION = "0.1.0"

# Logging configuration
logging.basicConfig(filename=config.logging, encoding="utf-8", level=logging.DEBUG, format=config.format)
# Retrieving absolut path for document root
config.document_root = Path(config.document_root).resolve()
sys.path.append(str(Path(config.plugins).resolve()))
plugins = []
for plugin_folder in Path(config.plugins).iterdir():
    if not plugin_folder.name.startswith("__") and (plugin_folder / "main.py").exists():
        logging.info(f"Plugin '{plugin_folder.name}' loaded")
        plugin = import_module(plugin_folder.name + ".main")
        try:
            plugin.init(config)
        except AttributeError as exception:
            logging.warning(exception)
            pass
        plugins.append(plugin)
# Sort plugins by priority
plugins = sorted(plugins, key=lambda x: getattr(x, "priority", 99))


def get_document_type(filename):
    EXTENSIONS = {
        "0": (".md", ".rst", ".txt"),  # text
        "4": (".hqx",),  # binhex - https://en.wikipedia.org/wiki/BinHex
        "g": (".apng", ".png"),  # png
        "I": (
            ".avif", ".bmp", ".gif", ".ico", ".jpg", ".jpeg", ".raw", ".tiff", ".webp",
            ".ai", ".eps", ".svg",
        ),  # images
    }
    extension = Path(filename).suffix.lower()
    for (document_type, extensions) in EXTENSIONS.items():
        if extension in extensions:
            return document_type
    # Unknown type is returned as binary
    return "9"


class Context:
    def __init__(self, values):
        for key, value in values.items():
            setattr(self, key, value)


class Client:
    def __init__(self, connection, ip_address):
        self.ip_address = ip_address
        self.socket = connection
        self.socket.setblocking(False)
        self.has_answer = False
        self.answer = ""
        self.route = "-"
        self.request_time = None
        self.status = "-"

    def request(self, route):
        self.request_time = datetime.now()
        self.has_answer = False
        if '\0' in route:
            logging.warning(f"NULL byte in route: {route}")
            return
        try:
            parts = route.encode("latin-1").decode("utf-8").splitlines()[0].split("\t")
        except UnicodeDecodeError:
            logging.warning(f"UnicodeDecodeError: {route}")
            parts = route.splitlines()[0].split("\t")
        route = parts[0] or "/"
        argument = "\t".join(parts[1:])
        self.route = f"{route}\t{argument}" if argument else f"{route}"
        context = Context(
            {
                "document_root": config.document_root,
                "hostname": config.hostname,
                "port": config.port,
                "route": route,
                "argument": argument,
            }
        )
        for plugin in plugins:
            if plugin.has_route(context):
                self.has_answer, self.answer, self.status = plugin.request(context)
                if self.has_answer:
                    return
        # Windows does not support slash at beginning of path
        if route.startswith("/"):
            document_path = config.document_root / route[1:]
        else:
            document_path = config.document_root / route
        # Securize document path
        if not document_path.is_relative_to(config.document_root):
            document_path = config.document_root
        if not document_path.exists():
            self.status = "3"
            self.answer = f"3'{route}' doesn't exist!\t\t\t\r\n.\r\n"
            self.answer = self.answer.encode("latin-1")
            self.has_answer = True
        elif document_path.is_dir():
            if route == "/":
                route = ""
            paths = document_path.iterdir()
            if config.sort:
                paths = sorted(list(paths), key=lambda p: p.name, reverse=True)
            for path in paths:
                if path.is_dir():
                    item_type = "1"
                    self.status = item_type
                    self.answer += "".join(
                        (
                            item_type,
                            path.name[:70] if config.strict else path.name,
                            "\t",
                            "/".join((route, path.name)),
                            "\t",
                            config.hostname,
                            "\t",
                            str(config.port),
                            "\r\n",
                        )
                    )
                elif path.is_file():
                    if not path.name.startswith("."):
                        name = path.name.encode("utf-8").decode("latin-1")
                        item_type = get_document_type(name)
                        self.status = item_type
                        self.answer += "".join(
                            (
                                f"{item_type}",
                                name[:70] if config.strict else name,
                                "\t",
                                "/".join((route, name)),
                                "\t",
                                f"{config.hostname}",
                                "\t",
                                f"{config.port}",
                                "\r\n",
                            )
                        )
            self.answer += ".\r\n"
            self.answer = self.answer.encode("latin-1")
            self.has_answer = True
        elif get_document_type(route) == "0":
            with document_path.open("rb") as file_handler:
                self.status = "0"
                self.answer = file_handler.read()
                self.answer += b".\r\n"
                self.has_answer = True
        else:
            self.status = "9"
            with document_path.open("rb") as file_handler:
                self.answer = file_handler.read()
                self.has_answer = True


class Server:
    def __init__(self):
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server_socket.setblocking(False)
        self.server_socket.bind((config.host, config.port))
        self.server_socket.listen()
        self.clients = []

    def start(self):
        logging.info(f"Starting gaufre v{VERSION}")
        while True:
            try:
                readers, writers, errors = select.select(
                    [self.server_socket] + [client.socket for client in self.clients],
                    [client.socket for client in self.clients],
                    [self.server_socket] + [client.socket for client in self.clients],
                )
                for reader in readers:
                    if reader is self.server_socket:
                        # Server socket : Accept new client
                        client_socket, address = self.server_socket.accept()
                        self.clients.append(Client(client_socket, address[0]))
                    else:
                        # Client socket : Read request string
                        try:
                            data_buffer = reader.recv(4096)
                        except ConnectionResetError as exception:
                            logging.warning(exception)
                            data_buffer = None
                        if data_buffer:
                            for client in self.clients:
                                if client.socket is reader:
                                    logging.debug(b"Request : " + data_buffer)
                                    client.request(data_buffer.decode("latin-1"))
                        else:
                            for client in self.clients:
                                if client.socket is reader:
                                    self.clients.remove(client)
                                    reader.close()
                                    break
                for writer in writers:
                    for client in self.clients:
                        if client.socket is writer:
                            if client.has_answer:
                                logging.info(" ".join((
                                    f"{client.ip_address}",
                                    f"[{client.request_time}]",
                                    f"{client.route}",
                                    f"{client.status}",
                                    f"{len(client.answer)}",
                                )))
                                # Big files should not block socket
                                BUFFER = 1048576
                                if len(client.answer) < BUFFER:
                                    writer.sendall(client.answer)
                                    self.clients.remove(client)
                                    writer.close()
                                else:
                                    sent = writer.send(client.answer[:BUFFER])
                                    if sent == 0:
                                        logging.warning("Socket connection broken")
                                        # ~ raise RuntimeError("socket connection broken")
                                        self.clients.remove(client)
                                        writer.close()
                                    client.answer = client.answer[BUFFER:]
            except KeyboardInterrupt:
                self.stop()
                break

    def stop(self):
        logging.info(f"Stopping gaufre")
        for client in self.clients:
            client[0].close()
        self.server_socket.close()


if __name__ == "__main__":
    Server().start()
